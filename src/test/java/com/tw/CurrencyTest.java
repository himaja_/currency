package com.tw;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CurrencyTest {

    Currency seventyFourPointEightFiveRupees;
    Currency oneDollar;
    Wallet wallet;

    @BeforeEach
    void setUp(){
        seventyFourPointEightFiveRupees= Currency.rupee(74.85);
        oneDollar= Currency.dollar(1);
        wallet= new Wallet();
    }

    @Test
    void shouldCheckEqualityWhenSeventyFourPointEightFiveRupeesAndOneDollarAreCompared(){
        assertThat(seventyFourPointEightFiveRupees, is(equalTo(oneDollar)));
    }

    @Test
    void shouldBeAbleToPutCertainAmountInTheWalletAndCheckTheValueOfMoneyInTheWallet(){
        Currency tenRupees= Currency.rupee(10);

        wallet.addMoney(tenRupees);

        assertThat(wallet.getValueInRupees(), is(equalTo(tenRupees)));
    }

    @Test
    void shouldReturnOneTwentyFourPointEightFiveRupeesWhenFiftyRupeesAndOneDollarAreAddedToTheWallet(){
        Currency fiftyRupees= Currency.rupee(50);
        Currency oneTwentyFourPointEightFiveRupees= Currency.rupee(124.85);

        wallet.addMoney(fiftyRupees);
        wallet.addMoney(oneDollar);

        assertThat(wallet.getValueInRupees(), is(equalTo(oneTwentyFourPointEightFiveRupees)));
    }

    @Test
    void shouldReturnFourDollarsWhenSeventyFourPointEightFiveRupeesAndOneDollarAndOneFortyNinePointSevenRupeesAreAddedToTheWallet(){
        Currency oneFortyNinePointSevenRupees= Currency.rupee(149.7);
        Currency fourDollars= Currency.dollar(4);

        wallet.addMoney(seventyFourPointEightFiveRupees);
        wallet.addMoney(oneDollar);
        wallet.addMoney(oneFortyNinePointSevenRupees);

        assertThat(wallet.getValueInDollars(), is(equalTo(fourDollars)));
    }

    @Test
    void shouldBeAbleToTakeOutOneDollarFromTheWalletWhenMoreThanOrEqualToOneDollarOfMoneyIsThereInTheWallet() throws OutOfMoneyException {
        Currency twoDollars= Currency.dollar(2);

        wallet.addMoney(twoDollars);

        assertThat(wallet.takeOut(oneDollar), is(equalTo(true)));
    }

    @Test
    void shouldBeAbleToTakeOutFiftyRupeesFromTheWalletWhenMoreThanOrEqualToFiftyRupeesOfMoneyIsThereInTheWallet() throws OutOfMoneyException {
        Currency fiftyRupees= Currency.rupee(50);

        wallet.addMoney(seventyFourPointEightFiveRupees);

        assertThat(wallet.takeOut(fiftyRupees), is(equalTo(true)));
    }

    @Test
    void shouldThrowOutOfMoneyExceptionWhenThereIsNotEnoughMoneyOfTheSpecificCurrencyTypeInTheWallet() {
        assertThrows(OutOfMoneyException.class, ()-> wallet.takeOut(oneDollar));
    }

}
