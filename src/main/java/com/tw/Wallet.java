package com.tw;

public class Wallet {
    private double rupees;
    private double dollars;

    public void addMoney(Currency money) {
        if(money.getCurrencyType()== Currency.CurrencyType.rupeeType)
            rupees += money.getValue();
        else
            dollars += money.getValue();
    }

    public Currency getValueInRupees(){
        double valueInRupees = rupees;
        valueInRupees += Currency.dollar(dollars).convertToBaseType(Currency.CurrencyType.rupeeType);
        return Currency.rupee(valueInRupees);
    }

    public Currency getValueInDollars() {
        double valueInDollars= dollars;
        valueInDollars += Currency.rupee(rupees).convertToBaseType(Currency.CurrencyType.dollarType);
        return Currency.dollar(valueInDollars);
    }

    public boolean takeOut(Currency money) throws OutOfMoneyException {
        if(money.getCurrencyType()== Currency.CurrencyType.rupeeType){
            if(money.getValue()<=rupees) {
                rupees -= money.getValue();
                return true;
            }
        }
        else{
            if(money.getValue()<=dollars){
                dollars -= money.getValue();
                return true;
            }
        }
        throw new OutOfMoneyException("Not enough money of the specified Currency type");
    }
}
