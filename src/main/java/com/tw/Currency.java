package com.tw;

import java.util.Objects;

public class Currency {
    private final double value;
    private final CurrencyType type;

    private Currency(double value, CurrencyType type){
        this.value= value;
        this.type= type;
    }

    public static class CurrencyType{
        private final double factor;
        private CurrencyType(double factor){
            this.factor= factor;
        }
        public static final CurrencyType rupeeType = new CurrencyType(1);
        public static final CurrencyType dollarType = new CurrencyType(74.85);
    }

    public static Currency rupee(double value) {
        return new Currency(value, CurrencyType.rupeeType);
    }

    public static Currency dollar(double value) {
        return new Currency(value, CurrencyType.dollarType);
    }

    public double convertToBaseType(CurrencyType type){
        return (this.value*this.type.factor)/type.factor;
    }

    public CurrencyType getCurrencyType(){
        return this.type;
    }

    public double getValue(){
        return this.value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency that = (Currency) o;
        return this.convertToBaseType(this.type)==that.convertToBaseType(this.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
