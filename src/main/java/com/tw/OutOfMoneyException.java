package com.tw;

public class OutOfMoneyException extends Exception{
    public OutOfMoneyException(String msg){
        super(msg);
    }
}
